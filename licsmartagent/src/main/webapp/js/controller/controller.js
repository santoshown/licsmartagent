

/*$============Index Controller===============$*/
function IndexController($scope,NewsService) {
	
	$scope.name="Index Controller";
	
	  $scope.salesData=[
	                    {hour: 1,sales: 54},
	                    {hour: 2,sales: 66},
	                    {hour: 3,sales: 77},
	                    {hour: 4,sales: 70},
	                    {hour: 5,sales: 60},
	                    {hour: 6,sales: 63},
	                    {hour: 7,sales: 55},
	                    {hour: 8,sales: 47},
	                    {hour: 9,sales: 55},
	                    {hour: 10,sales: 30}
	                ];
	               
	  $scope.options = {width: 500, height: 300, 'bar': 'aaa'};
      $scope.data = [1, 2, 3, 4];
      $scope.hovered = function(d){
          $scope.barValue = d;
          $scope.$apply();
      };
      $scope.barValue = 'None';
      
   
			      
	/*$scope.newsEntries = NewsService.query();
	$scope.deleteEntry = function(Entry) {
		Entry.$remove(function() {
			$scope.newsEntries  = NewsService.query();
		});
	};*/
};

/*$============Premium Controller===============$*/
function premiumController($scope, premiumService){
	$scope.form = premiumService.query();
	$scope.deleteEntry = function(Entr) {
		Entr.$remove(function success(data) {
			console.log("Deleted successfully");
			$scope.form  = premiumService.query();
		},function error(){
			console.log("Deleted unsuccessfully");
		});
	};
};



/*$============Premium Edit Controller===============$*/
function premiumEditController($scope, $routeParams, $location, premiumService) {
	$scope.form = premiumService.get({id : $routeParams.id});
	$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/premium');
		}, function error() {
			console.log("Saved unsuccessfully ");
		});
	};
};

/*$============Premium Create Controller===============$*/
function premiumCreateController($scope, $location,premiumService) {
	$scope.form = new premiumService();
	$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/premium');
		}, function error() {
			console.log("Saved unsuccessfully ");
		});
	};
};

/*$============Plan Controller===============$*/
function planController($scope, planService) {
	$scope.form =  planService.query();
	$scope.deleteEntry = function(Entr) {
		Entr.$remove(function success(data) {
			console.log("Deleted successfuuly");
			$scope.form  = planService.query();
		},function error(){
			console.log("Deleted unsuccessfully");
		});
	};
};

/*$============Plan Create Controller===============$*/
function planCreateController($scope, $location, planService) {
	$scope.form = new planService();
	$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/plan');
		}, function error() {
			console.log("Saved unsuccessfully ");
		});
	};
};

/*$============Plan Edit Controller===============$*/
function planEditController($scope, $routeParams, $location, planService) {
	$scope.form = planService.get({id : $routeParams.id});
	$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/plan');
		}, function error() {
			console.log("Saved unsuccessfully ");
		});
	};
};

/*$============Channel Type Controller===============$*/
function channelTypeController($scope, channelService) {
	$scope.form = channelService.query();
	$scope.deleteEntry = function(Entr) {
		Entr.$remove(function success() {
			console.log("Deleted successfuuly");
			$scope.form  = channelService.query();
		},function error(){
			console.log("Deleted unsuccessfuuly");
		});
	};
};

/*$============Channel Type Create Controller===============$*/
function channelTypeCreateController($scope, $location, channelService) {
	$scope.form = new channelService();
	$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/channelType');
		}, function error() {
			console.log("saved  unsuccessfully");
		});
	};
};

/*$============Channel Type Edit Controller===============$*/
function channelTypeEditController($scope, $routeParams, $location, channelService) {
	$scope.form = channelService.get({id : $routeParams.id});
		$scope.save = function() {
		$scope.form.$save(function success() {
			console.log("saved successfully");
			$location.path('/channelType');
		},function error(){
			console.log("saved  unsuccessfully");
		});
	};
};

/*$============Payment Type Controller===============$*/
function paymentTypeController($scope,paymentService) {
	$scope.form = paymentService.query();
	$scope.deleteEntry = function(Entr) {
		Entr.$remove(function success(data) {
			console.log("Deleted successfuuly");
			$scope.form  = paymentService.query();
		},function error(){
			console.log("Deleted unsuccessfuuly");
		});
	};
};

/*$============Payment Type Create Controller===============$*/
function paymentTypeCreateController($scope, $location, paymentService) {
	$scope.form = new paymentService();
	$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/paymentType');
		},function error(){
			console.log("saved unsuccessfully");
		});
	};
};

/*$============Payment Type Edit Controller===============$*/
function paymentTypeEditController($scope, $routeParams, $location, paymentService) {
	$scope.form = paymentService.get({id : $routeParams.id});
		$scope.save = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/paymentType');
		},function error(){
			console.log("saved unsuccessfully");
		});
	};
};


function EditController($scope, $routeParams, $location, NewsService) {
	$scope.newsEntry = NewsService.get({id : $routeParams.id});
		$scope.save = function() {
		$scope.newsEntry.$save(function() {
			$location.path('/');
		});
	};
};


function CreateController($scope, $location, NewsService) {
	$scope.newsEntry = new NewsService();
	$scope.save = function() {
		$scope.newsEntry.$save(function() {
			$location.path('/');
		});
	};
};
function adminDashboardController($scope) {
	$scope.name="Admin Dash Board";
	//$scope.newsEntry = new NewsService();
	/*$scope.save = function() {
		$scope.newsEntry.$save(function() {
			$location.path('/');
		});
	};*/
};


function registerController($scope, $location, registerService) {

    $scope.monthSelectorOptions = {
            start: "year",
            depth: "year"
          };
          $scope.getType = function(x) {
            return typeof x;
          };
          $scope.isDate = function(x) {
            return x instanceof Date;
          };

	$scope.form = new registerService();
	$scope.registerSave = function() {
		$scope.form.$save(function success(data) {
			console.log("saved successfully");
			$location.path('/');
		}, function error() {
			console.log("saved unsuccessfully");
		});
	};
			
};



function LoginController($scope, $rootScope, $location, $cookieStore,
		UserService,loginValidationService) {

	$scope.rememberMe = false;

	$scope.login = function() {
	        // alert("function called");
		$scope.submitted = true;
			$rootScope.username = $scope.username;
	        $rootScope.password = $scope.password;
	        var result = loginValidationService.loginForm();
	        if (result == false) {
	          return false;
	        }
	        $rootScope.username="";
	        $rootScope.password="";
		UserService.authenticate($.param({
			username : $scope.username,
			password : $scope.password
		}), function(authenticationResult) {
			var authToken = authenticationResult.token;
			/* console.log=authToken; */
			$rootScope.authToken = authToken;
			if ($scope.rememberMe) {
				$cookieStore.put('authToken', authToken);
			}
			UserService.get(function(user) {
				$rootScope.user = user;
				if(user.roles.admin){
				//	alert(user.roles.admin);
					$location.path("/adminDashboard");
				}
				else{
					$location.path("/");
				 }
			});
		});
	};
};

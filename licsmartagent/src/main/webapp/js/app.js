var app =  angular.module('exampleApp', ['kendo.directives','ngRoute', 'ngCookies', 'exampleApp.services','ui.bootstrap'])
	.config(
		[ '$routeProvider', '$locationProvider', '$httpProvider', function($routeProvider, $locationProvider, $httpProvider) {

			
			$routeProvider.when('/login', {
				templateUrl: 'pages/login.html',
				controller: LoginController
			});
			
			$routeProvider.when('/create', {
				templateUrl: 'pages/create.html',
				controller: CreateController
			});
			
			$routeProvider.when('/adminDashboard', {
				templateUrl: 'pages/adminDashboard.html',
				controller: adminDashboardController
			});
			
			$routeProvider.when('/premiumCreate', {
				templateUrl: 'pages/premiumCreate.html',
				controller: premiumCreateController
			});
			
			$routeProvider.when('/planCreate', {
				templateUrl: 'pages/planCreate.html',
				controller: planCreateController
			});
			
			$routeProvider.when('/channelTypeCreate', {
				templateUrl: 'pages/channelTypeCreate.html',
				controller: channelTypeCreateController
			});
			
			$routeProvider.when('/paymentTypeCreate', {
				templateUrl: 'pages/paymentTypeCreate.html',
				controller: paymentTypeCreateController
			});
			
			
			$routeProvider.when('/edit/:id', {
				templateUrl: 'pages/edit.html',
				controller: EditController
			});

			$routeProvider.when('/premiumEdit/:id', {
				templateUrl: 'pages/premiumEdit.html',
				controller: premiumEditController
			});
			
			$routeProvider.when('/planEdit/:id', {
				templateUrl: 'pages/planEdit.html',
				controller: planEditController
			});
			
			$routeProvider.when('/channelTypeEdit/:id', {
				templateUrl: 'pages/channelTypeEdit.html',
				controller: channelTypeEditController
			});
			
			$routeProvider.when('/paymentTypeEdit/:id', {
				templateUrl: 'pages/paymentTypeEdit.html',
				controller: paymentTypeEditController
			});
			
			$routeProvider.when('/register', {
				templateUrl: 'pages/registerPage.html',
				controller: registerController
			});
			
			$routeProvider.when('/premium', {
				templateUrl: 'pages/premium.html',
				controller: premiumController
			});
			
			$routeProvider.when('/plan', {
				templateUrl: 'pages/plan.html',
				controller: planController
			});
			
			$routeProvider.when('/channelType', {
				templateUrl: 'pages/channelType.html',
				controller: channelTypeController
			});
			
			$routeProvider.when('/paymentType', {
				templateUrl: 'pages/paymentType.html',
				controller: paymentTypeController
			});
			
			$routeProvider.otherwise({
				templateUrl: 'pages/index.html',
				controller: IndexController
			});
			
			$locationProvider.hashPrefix('!');
			


			
			/* Register error provider that shows message on failed requests or redirects to login page on
			 * unauthenticated requests */
		    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
			        return {
			        	'responseError': function(rejection) {
			        		var status = rejection.status;
			        		var config = rejection.config;
			        		var method = config.method;
			        		var url = config.url;
			      
			        		if (status == 401) {
			        			$location.path( "/login" );
			        			$rootScope.error = " User Name or Password is InValid....?";
			        		} else {
			        			
			        			$rootScope.error = " User Name or Password is InValid....?";
			        		}
			        		if (status == 500) {
			        			/*$location.path( "/login" );*/
			        			$rootScope.error =   status + " Internal Server Error! ";
			        		} else {
			        			$location.path( "/login" );
			        			$rootScope.error = method + " on " + url + " failed with status " + status;
			        		}
			        		return $q.reject(rejection);
			        	}
			        };
			    }
		    );
		    
		    /* Registers auth token interceptor, auth token is either passed by header or by query parameter
		     * as soon as there is an authenticated user */
		    $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
		        return {
		        	'request': function(config) {
		        		var isRestCall = config.url.indexOf('rest') == 0;
		        		if (isRestCall && angular.isDefined($rootScope.authToken)) {
		        			var authToken = $rootScope.authToken;
		        			if (exampleAppConfig.useAuthTokenHeader) {
		        				config.headers['X-Auth-Token'] = authToken;
		        			} else {
		        				config.url = config.url + "?token=" + authToken;
		        			}
		        		}
		        		return config || $q.when(config);
		        	}
		        };
		    }
	    );
		   
//		    $tooltipProvider.setTriggers({
//			    'mouseenter': 'mouseleave',
//			    'click': 'click',
//			    'focus': 'blur',
//			    'never': 'mouseleave' // <- This ensures the tooltip will go away on mouseleave
//			  });
		    
		} ]
		
	).run(function($rootScope, $location, $cookieStore, UserService) {
		
		/* Reset error when a new view is loaded */
		$rootScope.$on('$viewContentLoaded', function() {
			delete $rootScope.error;
		});
		
		$rootScope.hasRole = function(role) {
			
			if ($rootScope.user === undefined) {
				return false;
			}
			
			if ($rootScope.user.roles[role] === undefined) {
				return false;
			}
			
			return $rootScope.user.roles[role];
		};
		
		$rootScope.logout = function() {
			delete $rootScope.user;
			delete $rootScope.authToken;
			$cookieStore.remove('authToken');
			$location.path("/login");
		};
		
		 /* Try getting valid user from cookie or go to login page */
		var originalPath = $location.path();
		$location.path("/login");
		var authToken = $cookieStore.get('authToken');
		if (authToken !== undefined) {
			$rootScope.authToken = authToken;
			UserService.get(function(user) {
				$rootScope.user = user;
				$location.path(originalPath);
			});
		}
		$rootScope.initialized = true;
	});




  

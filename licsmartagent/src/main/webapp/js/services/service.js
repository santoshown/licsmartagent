
var services = angular.module('exampleApp.services', ['ngResource']);

services.factory('UserService', function($resource) {

	return $resource('rest/user/:action', {}, {
		authenticate : {
			method : 'POST',
			params : {
				'action' : 'authenticate'
			},
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		},
	});
});

services.factory('NewsService', function($resource) {

	return $resource('rest/success:id', {
		id : '@id'
	});
});

services.factory('registerService', function($resource) {

	return $resource('rest/user/register:id', {id : '@id'});
});


services.factory('premiumService', function($resource) {

	return $resource('rest/admin/premium/:id', {id : '@premiumID'});
});

services.factory('planService', function($resource) {

	return $resource('rest/admin/plan/:id', {id : '@planID'});
});

services.factory('channelService', function($resource) {

	return $resource('rest/admin/channel/:id', {id : '@channelID'});
});


services.factory('paymentService', function($resource) {

	return $resource('rest/admin/payment/:id', {id : '@paymentTypeID'});
});

/*$==============login page validation ================$*/
app.service('loginValidationService', function($rootScope){
	this.loginForm = function() {
		var username = $rootScope.username;
		var password = $rootScope.password;
		if ((username == null) || (username == "")) {
			$rootScope.uerror = "Please enter email ID....!";
			return false;
		}
		$rootScope.uerror = "";
		
		if ((password == null) || (password == "")) {
			$rootScope.passerror = "Please enter password...!";
			return false;
		}
		$rootScope.passerror = "";
	};
});
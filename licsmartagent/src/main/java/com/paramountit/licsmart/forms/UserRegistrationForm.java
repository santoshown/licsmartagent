package com.paramountit.licsmart.forms;

import java.sql.Date;

public class UserRegistrationForm {

	private Long id;
	private String userName;
	private String password;
	private String firstName;
	private String mName;
	private String lastName;
	private Long phone;
	private String address;
	private Date addedOn;
	private String reporting;
	private String dob;
	private String agentNumber;
	private String emailID;
	private Integer isActive;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public String getReporting() {
		return reporting;
	}

	public void setReporting(String reporting) {
		this.reporting = reporting;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getAgentNumber() {
		return agentNumber;
	}

	public void setAgentNumber(String agentNumber) {
		this.agentNumber = agentNumber;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	@Override
	public String toString() {
		return "UserRegistrationForm [id=" + id + ", userName=" + userName
				+ ", password=" + password + ", firstName=" + firstName
				+ ", mName=" + mName + ", lastName=" + lastName + ", phone="
				+ phone + ", address=" + address + ", addedOn=" + addedOn
				+ ", reporting=" + reporting + ", dob=" + dob
				+ ", agentNumber=" + agentNumber + ", emailID=" + emailID
				+ ", isActive=" + isActive + "]";
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

}

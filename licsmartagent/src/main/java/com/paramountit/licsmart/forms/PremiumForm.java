package com.paramountit.licsmart.forms;

public class PremiumForm {

	private Integer premiumID;
	private String premium;

	public Integer getPremiumID() {
		return premiumID;
	}

	public void setPremiumID(Integer premiumID) {
		this.premiumID = premiumID;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

	@Override
	public String toString() {
		return "PremiumForm [premiumID=" + premiumID + ", premium=" + premium
				+ "]";
	}

}

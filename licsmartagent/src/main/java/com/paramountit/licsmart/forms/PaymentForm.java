package com.paramountit.licsmart.forms;

public class PaymentForm {

	private Integer paymentTypeID;

	public Integer getPaymentTypeID() {
		return paymentTypeID;
	}

	public void setPaymentTypeID(Integer paymentTypeID) {
		this.paymentTypeID = paymentTypeID;
	}

	@Override
	public String toString() {
		return "PaymentForm [paymentTypeID=" + paymentTypeID + ", paymentType="
				+ paymentType + "]";
	}

	private String paymentType;

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

}

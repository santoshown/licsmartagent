package com.paramountit.licsmart.forms;

public class ChannelForm {

	private Integer channelID;

	public Integer getChannelID() {
		return channelID;
	}

	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}

	private String ChannelName;

	public String getChannelName() {
		return ChannelName;
	}

	@Override
	public String toString() {
		return "ChannelForm [channelID=" + channelID + ", ChannelName="
				+ ChannelName + "]";
	}

	public void setChannelName(String channelName) {
		ChannelName = channelName;
	}
}

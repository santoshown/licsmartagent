package com.paramountit.licsmart.forms;

public class PlanForm {

	private Integer planID;
	private String planName;
	private String description;
	private Integer isActive;

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	public Integer getPlanID() {
		return planID;
	}

	public void setPlanID(Integer planID) {
		this.planID = planID;
	}

	@Override
	public String toString() {
		return "PlanForm [planID=" + planID + ", planName=" + planName
				+ ", description=" + description + ", isActive=" + isActive
				+ "]";
	}

}

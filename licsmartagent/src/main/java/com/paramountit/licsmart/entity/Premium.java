package com.paramountit.licsmart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MstPremium")
public class Premium implements Serializable {
private static final long serialVersionUID = 1L;

	public Premium() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PremiumID", nullable = false, unique = true)
	private Integer premiumID;

	@Column(name = "Premium")
	private String premium;

	@Override
	public String toString() {
		return "Premium [premiumID=" + premiumID + ", premium=" + premium + "]";
	}

	public Integer getPremiumID() {
		return premiumID;
	}

	public void setPremiumID(Integer premiumID) {
		this.premiumID = premiumID;
	}

	public String getPremium() {
		return premium;
	}

	public void setPremium(String premium) {
		this.premium = premium;
	}

}

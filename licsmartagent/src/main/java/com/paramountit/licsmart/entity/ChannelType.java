package com.paramountit.licsmart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MstChannelType")
public class ChannelType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ChannelID")
	private Integer channelID;

	@Column(name = "ChannelName")
	private String channelName;

	@Override
	public String toString() {
		return "ChannelType [channelID=" + channelID + ", channelName="
				+ channelName + "]";
	}

	public ChannelType(Integer channelID, String channelName) {
		super();
		this.channelID = channelID;
		this.channelName = channelName;
	}

	public ChannelType() {
		super();
	}

	public Integer getChannelID() {
		return channelID;
	}

	public void setChannelID(Integer channelID) {
		this.channelID = channelID;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}
}

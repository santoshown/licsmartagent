package com.paramountit.licsmart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MstPaymentType")
public class PaymentType implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PaymentTypeID")
	private Integer paymentTypeID;

	@Column(name = "PaymentType")
	private String paymentType;

	public Integer getPaymentTypeID() {
		return paymentTypeID;
	}

	@Override
	public String toString() {
		return "PaymentType [paymentTypeID=" + paymentTypeID + ", paymentType="
				+ paymentType + "]";
	}

	public void setPaymentTypeID(Integer paymentTypeID) {
		this.paymentTypeID = paymentTypeID;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

}

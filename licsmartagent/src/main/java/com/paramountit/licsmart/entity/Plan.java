package com.paramountit.licsmart.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MstPlan")
public class Plan implements Serializable {
private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PlanID")
	private Integer planID;

	public Plan() {
		super();
	}

	@Column(name = "PlanName")
	private String planName;

	@Column(name = "Description")
	private String description;

	@Column(name = "IsActive")
	private Integer isActive;

	@Override
	public String toString() {
		return "Plan [planID=" + planID + ", planName=" + planName
				+ ", description=" + description + ", isActive=" + isActive
				+ "]";
	}

	public Integer getPlanID() {
		return planID;
	}

	public void setPlanID(Integer planID) {
		this.planID = planID;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getIsActive() {
		return isActive;
	}

	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

}

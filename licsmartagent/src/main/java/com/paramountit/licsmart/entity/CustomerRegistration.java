package com.paramountit.licsmart.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MstCustomerRegistration")
public class CustomerRegistration implements Serializable {

	private static final long serialVersionUID = 1L;

	public CustomerRegistration() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "CustomerID")
	private Integer customerID;

	@Column(name = "FirstName")
	private String firstName;

	@Column(name = "Mname")
	private String middleName;

	@Column(name = "LastName")
	private String lastName;

	@Column(name = "PolicyNo")
	private String policyNumber;

	@Column(name = "DOC")
	private Date dateOfcommencement;

	@Column(name = "SumAssured")
	private Float sumAssured;

	@Column(name = "Nom")
	private String nominee;

	@Column(name = "AgentName")
	private String agentName;

	@Column(name = "DOB")
	private Date dateOfBirth;

	@Column(name = "EmailID")
	private String emailID;

	@Column(name = "Phone")
	private Number phone;

	@Column(name = "AddedOn")
	private Date addedOn;

	@Column(name = "Address")
	private String address;

	@Column(name = "IsActive")
	private Integer isActive;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "EnrollerID", insertable = true, updatable = true)
	private User user;

	public Integer getCustomerID() {
		return customerID;
	}

	public void setCustomerID(Integer customerID) {
		this.customerID = customerID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Date getDateOfcommencement() {
		return dateOfcommencement;
	}

	public void setDateOfcommencement(Date dateOfcommencement) {
		this.dateOfcommencement = dateOfcommencement;
	}

	public Float getSumAssured() {
		return sumAssured;
	}

	public void setSumAssured(Float sumAssured) {
		this.sumAssured = sumAssured;
	}

	public String getNominee() {
		return nominee;
	}

	public void setNominee(String nominee) {
		this.nominee = nominee;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public Number getPhone() {
		return phone;
	}

	public void setPhone(Number phone) {
		this.phone = phone;
	}

	public Date getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(Date addedOn) {
		this.addedOn = addedOn;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Premium getPremium() {
		return premium;
	}

	public void setPremium(Premium premium) {
		this.premium = premium;
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}

	public PaymentType getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(PaymentType paymentType) {
		this.paymentType = paymentType;
	}

	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PremiumID", insertable = true, updatable = true)
	private Premium premium;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "plan_id", insertable = true, updatable = true)
	private Plan plan;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PaymentTypeID", insertable = true, updatable = true)
	private PaymentType paymentType;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ChannelID", insertable = true, updatable = true)
	private ChannelType channelType;

	@Override
	public String toString() {
		return "CustomerRegistration [customerID=" + customerID
				+ ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", policyNumber=" + policyNumber
				+ ", dateOfcommencement=" + dateOfcommencement
				+ ", sumAssured=" + sumAssured + ", nominee=" + nominee
				+ ", agentName=" + agentName + ", dateOfBirth=" + dateOfBirth
				+ ", emailID=" + emailID + ", phone=" + phone + ", addedOn="
				+ addedOn + ", user=" + user + ", premium=" + premium
				+ ", plan=" + plan + ", paymentType=" + paymentType
				+ ", channelType=" + channelType + "]";
	}

}

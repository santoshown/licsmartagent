package com.paramountit.licsmart.controller;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import com.paramountit.licsmart.JsonViews;
import com.paramountit.licsmart.entity.ChannelType;
import com.paramountit.licsmart.forms.ChannelForm;
import com.paramountit.licsmart.services.ChannelService;

@Controller
@Path("/admin/channel")
public class ChannelController {

	@Autowired
	private ChannelService ChannelService;

	@Autowired
	private ObjectMapper mapper;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list() throws JsonGenerationException, JsonMappingException,
			IOException {
		this.logger.info("Channel list()");
		ObjectWriter viewWriter;
		if (isAdmin()) {
			viewWriter = mapper.writerWithView(JsonViews.Admin.class);
		} else {
			viewWriter = mapper.writerWithView(JsonViews.User.class);
		}

		List<ChannelType> allPaymentTypes = ChannelService.findAll();

		return viewWriter.writeValueAsString(allPaymentTypes);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public ChannelType getChannel(@PathParam("id") Integer id) {
		logger.info("getChannel " + id);
		ChannelType paymentType = ChannelService.getChannel(id);
		if (paymentType == null) {

			throw new WebApplicationException(404);
		}

		return paymentType;

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public ChannelType save(ChannelForm form) {
		logger.info("save () " + form);
		ChannelType paymentType = ChannelService.save(form);
		return paymentType;

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public ChannelType update(@PathParam("id") Integer id, ChannelForm form) {
		logger.info("Channel update " + id);
		ChannelType paymentType = ChannelService.update(form);
		return paymentType;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public void delete(@PathParam("id") Integer id) {
		logger.info("payment delete " + id);
		ChannelService.delete(id);
	}

	private boolean isAdmin() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof String
				&& ((String) principal).equals("anonymousUser")) {
			return false;
		}
		UserDetails userDetails = (UserDetails) principal;

		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			if (authority.toString().equals("admin")) {
				return true;
			}
		}

		return false;
	}

}

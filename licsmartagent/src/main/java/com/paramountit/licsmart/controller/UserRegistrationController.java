package com.paramountit.licsmart.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.text.ParseException;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.paramountit.licsmart.dao.user.JpaUserDao;
import com.paramountit.licsmart.entity.User;
import com.paramountit.licsmart.forms.UserRegistrationForm;
import com.paramountit.licsmart.services.UserRegistrationService;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Controller
@Path("user/register")
public class UserRegistrationController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private UserRegistrationService userRegistrationService;

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User createUser(UserRegistrationForm form) throws ParseException {
		logger.info("Registration fields" + form);
		Boolean userName = userRegistrationService.checkUserExist(form);
		if (!userName) {

			throw new WebApplicationException(302);
		}
		User user = userRegistrationService.createUser(form);
		return user;

	}

	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("image")
	public Response uploadImage(@FormDataParam("file") InputStream is,
			@FormDataParam("file") FormDataContentDisposition detail) throws IOException {
		detail.getName();
		byte[] imageArray=IOUtils.toByteArray(is);
		return null;

	}

}

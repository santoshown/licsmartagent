package com.paramountit.licsmart.controller;

import java.io.IOException;
import java.util.*;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.paramountit.licsmart.JsonViews;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Path("/success")
public class HomeController {

	@Autowired
	private ObjectMapper mapper;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String login() throws JsonGenerationException, JsonMappingException,
			IOException {

		ObjectWriter viewWriter;
		viewWriter = this.mapper.writerWithView(JsonViews.User.class);
		String msg = "Login successfull";
		List<String> list = new ArrayList<String>();

		list.add(msg);

		return viewWriter.writeValueAsString(list);
	}

}

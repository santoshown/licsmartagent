package com.paramountit.licsmart.controller;

import java.io.IOException;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import com.paramountit.licsmart.JsonViews;
import com.paramountit.licsmart.entity.Premium;
import com.paramountit.licsmart.forms.PremiumForm;
import com.paramountit.licsmart.services.PremiumService;

@Controller
@Path("/admin/premium")
public class PremiumController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PremiumService premiumService;

	@Autowired
	private ObjectMapper mapper;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list() throws JsonGenerationException, JsonMappingException,
			IOException {
		logger.info("list() ");
		ObjectWriter viewWriter;
		if (isAdmin()) {
			viewWriter = mapper.writerWithView(JsonViews.Admin.class);
		} else {
			viewWriter = mapper.writerWithView(JsonViews.User.class);
		}
		List<Premium> allPremium = premiumService.findAll();

		return viewWriter.writeValueAsString(allPremium);

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Premium create(PremiumForm form) {

		logger.info("create ()" + form);
		Premium premium = premiumService.create(form);
		return premium;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Premium getPremium(@PathParam("id") Integer id) {

		Premium premium = premiumService.getPremium(id);
		if (premium == null) {

			throw new WebApplicationException(404);
		}

		return premium;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Premium update(@PathParam("id") Integer id, PremiumForm form) {
		logger.info("update " + id);
		Premium premium = premiumService.update(form);
		return premium;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public void delete(@PathParam("id") Integer id) {

		logger.info("delete " + id);
		premiumService.delete(id);
	}

	private boolean isAdmin() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof String
				&& ((String) principal).equals("anonymousUser")) {
			return false;
		}
		UserDetails userDetails = (UserDetails) principal;

		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			if (authority.toString().equals("admin")) {
				return true;
			}
		}

		return false;
	}

}

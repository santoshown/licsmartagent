package com.paramountit.licsmart.controller;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import com.paramountit.licsmart.JsonViews;
import com.paramountit.licsmart.entity.PaymentType;
import com.paramountit.licsmart.services.PaymentService;
import com.paramountit.licsmart.forms.PaymentForm;

@Controller
@Path("/admin/payment")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private ObjectMapper mapper;

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String list() throws JsonGenerationException, JsonMappingException,
			IOException {
		this.logger.info("Payment list()");
		ObjectWriter viewWriter;
		if (isAdmin()) {
			viewWriter = mapper.writerWithView(JsonViews.Admin.class);
		} else {
			viewWriter = mapper.writerWithView(JsonViews.User.class);
		}

		List<PaymentType> allPaymentTypes = paymentService.findAll();

		return viewWriter.writeValueAsString(allPaymentTypes);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public PaymentType getPayment(@PathParam("id") Integer id) {
		logger.info("getPayment " + id);
		PaymentType paymentType = paymentService.getPaymentType(id);
		if (paymentType == null) {

			throw new WebApplicationException(404);
		}

		return paymentType;

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public PaymentType save(PaymentForm form) {
		logger.info("save () " + form);
		PaymentType paymentType = paymentService.save(form);
		return paymentType;

	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public PaymentType update(@PathParam("id") Integer id, PaymentForm form) {
		logger.info("payment update " + id);
		PaymentType paymentType = paymentService.update(form);
		return paymentType;
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public void delete(@PathParam("id") Integer id) {
		logger.info("payment delete " + id);
		paymentService.delete(id);
	}

	private boolean isAdmin() {
		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		Object principal = authentication.getPrincipal();
		if (principal instanceof String
				&& ((String) principal).equals("anonymousUser")) {
			return false;
		}
		UserDetails userDetails = (UserDetails) principal;

		for (GrantedAuthority authority : userDetails.getAuthorities()) {
			if (authority.toString().equals("admin")) {
				return true;
			}
		}

		return false;
	}

}

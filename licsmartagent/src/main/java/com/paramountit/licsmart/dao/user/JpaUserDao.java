package com.paramountit.licsmart.dao.user;

import java.util.ArrayList;
import java.util.List;
import com.paramountit.licsmart.entity.User;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

public class JpaUserDao implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = this.findByName(username);
		if (null == user) {
			throw new UsernameNotFoundException("The user with name "
					+ username + " was not found");
		}

		return user;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public User findByName(String name) {
		List<User> users = new ArrayList<User>();
		users = sessionFactory.getCurrentSession()
				.createQuery("from User where userName=?")
				.setParameter(0, name).list();
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
	}

}

package com.paramountit.licsmart.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.paramountit.licsmart.entity.Plan;
import com.paramountit.licsmart.forms.PlanForm;

@Repository
public class PlanDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Plan> findAll() {
		return sessionFactory.getCurrentSession().createQuery("From Plan")
				.list();
	}

	@Transactional(readOnly = true)
	public Plan find(Integer id) {
		return (Plan) sessionFactory.getCurrentSession().get(Plan.class, id);
	}

	@Transactional
	public Plan save(PlanForm form) {
		Plan plan = new Plan();
		plan.setPlanName(form.getPlanName());
		plan.setDescription(form.getDescription());
		plan.setIsActive(form.getIsActive());
		Session session = sessionFactory.getCurrentSession();
		session.save(plan);
		return plan;
	}

	@Transactional
	public Plan update(PlanForm form) {
		Plan plan = new Plan();
		plan.setPlanID(form.getPlanID());
		plan.setPlanName(form.getPlanName());
		plan.setDescription(form.getDescription());
		plan.setIsActive(form.getIsActive());
		Session session = sessionFactory.getCurrentSession();
		session.update(plan);
		return plan;
	}

	@Transactional
	public void delete(Integer id) {
		if (id == null) {
			return;
		}
		Plan plan = this.find(id);
		if (plan == null) {
			return;
		}
		sessionFactory.getCurrentSession().delete(plan);
	}
}

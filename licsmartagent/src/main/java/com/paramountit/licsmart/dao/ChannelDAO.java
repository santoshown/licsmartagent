package com.paramountit.licsmart.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.paramountit.licsmart.entity.ChannelType;
import com.paramountit.licsmart.forms.ChannelForm;

@Repository
public class ChannelDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<ChannelType> findAll() {
		return sessionFactory.getCurrentSession()
				.createQuery("From ChannelType").list();

	}

	@Transactional(readOnly = true)
	public ChannelType find(Integer id) {
		return (ChannelType) sessionFactory.getCurrentSession().get(
				ChannelType.class, id);

	}

	@Transactional
	public ChannelType save(ChannelForm form) {
		ChannelType channelType = new ChannelType();
		channelType.setChannelName(form.getChannelName());
		Session session = sessionFactory.getCurrentSession();
		session.save(channelType);
		return channelType;
	}

	@Transactional
	public ChannelType update(ChannelForm form) {
		ChannelType channelType = new ChannelType();
		channelType.setChannelID(form.getChannelID());
		channelType.setChannelName(form.getChannelName());
		Session session = sessionFactory.getCurrentSession();
		session.update(channelType);
		return channelType;
	}

	@Transactional
	public void delete(Integer id) {
		if (id == null) {
			return;
		}
		ChannelType channelType = this.find(id);
		if (channelType == null) {
			return;
		}
		sessionFactory.getCurrentSession().delete(channelType);
	}

}

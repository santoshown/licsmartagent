package com.paramountit.licsmart.dao.user;

import com.paramountit.licsmart.entity.User;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserDao extends UserDetailsService {

	User findByName(String name);

}
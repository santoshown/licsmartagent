package com.paramountit.licsmart.dao;

import java.sql.Date;
import java.text.ParseException;
import java.util.Calendar;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import com.paramountit.licsmart.entity.User;
import com.paramountit.licsmart.forms.UserRegistrationForm;

@Repository
public class UserRegistrationDAO {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public Boolean checkUserExist(UserRegistrationForm form) {
		String userName = form.getUserName();
		logger.info("userName " + userName);
		Criteria c = sessionFactory.getCurrentSession().createCriteria(
				User.class);
		c.add(Restrictions.ilike("userName", "%" + userName + "%"));
		int Result = ((Number) c.setProjection(Projections.rowCount())
				.uniqueResult()).intValue();
		logger.info("Query Result " + Result);
		if (Result <= 0) {
			return true;
		}
		return false;
	}

	@Transactional
	public User createUser(UserRegistrationForm form) throws ParseException {
		logger.info("R form" + form);
		User user = new User();
		user.setUserName(form.getUserName());
		user.setPassword(passwordEncoder.encode(form.getPassword()));
		user.addRole("user");
		user.setFirstName(form.getFirstName());
		user.setmName(form.getmName());
		user.setLastName(form.getLastName());
		user.setAddress(form.getAddress());
		user.setPhone(form.getPhone());
		user.setAddedOn(getCurrentDate());
		user.setDob(dateConversion(form.getDob()));
		user.setIsActive(form.getIsActive());
		user.setEmailID(form.getEmailID());
		user.setReporting(form.getReporting());
		user.setAgentNumber(form.getAgentNumber());
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
		return user;
	}

	public Date dateConversion(String date) throws ParseException {
		java.util.Date ud = new java.text.SimpleDateFormat("dd/MM/yyyy")
				.parse(date);
		java.sql.Date sd = new java.sql.Date(ud.getTime());
		return sd;
	}

	public Date getCurrentDate() {
		Date date = new Date(Calendar.getInstance().getTimeInMillis());
		return date;
	}

}

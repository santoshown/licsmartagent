package com.paramountit.licsmart.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.paramountit.licsmart.entity.PaymentType;
import com.paramountit.licsmart.forms.PaymentForm;

@Repository
public class PaymentDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<PaymentType> findAll() {
		return sessionFactory.getCurrentSession()
				.createQuery("From PaymentType ").list();
	}

	@Transactional
	public PaymentType find(Integer id) {

		return (PaymentType) sessionFactory.getCurrentSession().get(
				PaymentType.class, id);

	}

	@Transactional
	public PaymentType save(PaymentForm form) {
		PaymentType paymentType = new PaymentType();
		paymentType.setPaymentType(form.getPaymentType());
		Session session = sessionFactory.getCurrentSession();
		session.save(paymentType);
		return paymentType;
	}

	@Transactional
	public PaymentType update(PaymentForm form) {
		PaymentType paymentType = new PaymentType();
		paymentType.setPaymentTypeID(form.getPaymentTypeID());
		paymentType.setPaymentType(form.getPaymentType());
		Session session = sessionFactory.getCurrentSession();
		session.update(paymentType);
		return paymentType;

	}

	@Transactional
	public void delete(Integer id) {
		if (id == null) {
			return;
		}
		PaymentType paymentType = this.find(id);
		if (paymentType == null) {
			return;
		}
		sessionFactory.getCurrentSession().delete(paymentType);
	}
}

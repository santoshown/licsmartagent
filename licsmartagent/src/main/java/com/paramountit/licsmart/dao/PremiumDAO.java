package com.paramountit.licsmart.dao;

import java.util.List;

import javax.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.paramountit.licsmart.entity.Premium;
import com.paramountit.licsmart.forms.PremiumForm;

@Repository
public class PremiumDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Premium> findAll() {
		return sessionFactory.getCurrentSession().createQuery("From Premium")
				.list();
	}

	@Transactional
	public Premium find(Integer id) {
		return (Premium) sessionFactory.getCurrentSession().get(Premium.class,
				id);
	}

	@Transactional
	public Premium save(PremiumForm form) {
		Premium premium = new Premium();
		premium.setPremium(form.getPremium());
		Session session = sessionFactory.getCurrentSession();
		session.save(premium);
		return premium;
	}

	@Transactional
	public Premium update(PremiumForm form) {
		Premium premium = new Premium();
		premium.setPremiumID(form.getPremiumID());
		premium.setPremium(form.getPremium());
		Session session = sessionFactory.getCurrentSession();
		session.update(premium);
		return premium;
	}

	@Transactional
	public void delete(Integer id) {
		if (id == null) {
			return;
		}
		Premium premium = this.find(id);
		if (premium == null) {
			return;
		}
		sessionFactory.getCurrentSession().delete(premium);
	}

}

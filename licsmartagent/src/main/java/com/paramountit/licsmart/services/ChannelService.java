package com.paramountit.licsmart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paramountit.licsmart.dao.ChannelDAO;
import com.paramountit.licsmart.entity.ChannelType;
import com.paramountit.licsmart.forms.ChannelForm;

@Service
public class ChannelService {

	@Autowired
	private ChannelDAO ChannelType;

	public List<ChannelType> findAll() {

		return ChannelType.findAll();
	}

	public ChannelType getChannel(Integer id) {

		return ChannelType.find(id);
	}

	public ChannelType save(ChannelForm form) {

		return ChannelType.save(form);
	}

	public ChannelType update(ChannelForm form) {

		return ChannelType.update(form);
	}

	public void delete(Integer id) {

		ChannelType.delete(id);
	}

}

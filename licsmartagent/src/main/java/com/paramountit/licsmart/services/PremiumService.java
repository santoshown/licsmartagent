package com.paramountit.licsmart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paramountit.licsmart.dao.PremiumDAO;
import com.paramountit.licsmart.entity.Premium;
import com.paramountit.licsmart.forms.PremiumForm;

@Service
public class PremiumService {

	@Autowired
	private PremiumDAO premiumDAO;

	public List<Premium> findAll() {

		return premiumDAO.findAll();

	}

	public Premium getPremium(Integer id) {

		return premiumDAO.find(id);
	}

	public Premium create(PremiumForm form) {

		return premiumDAO.save(form);
	}

	public Premium update(PremiumForm form)

	{
		return premiumDAO.update(form);
	}

	public void delete(Integer id) {

		premiumDAO.delete(id);
	}
}

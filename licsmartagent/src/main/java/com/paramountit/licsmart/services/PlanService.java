package com.paramountit.licsmart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paramountit.licsmart.dao.PlanDAO;
import com.paramountit.licsmart.entity.Plan;
import com.paramountit.licsmart.forms.PlanForm;

@Service
public class PlanService {

	@Autowired
	private PlanDAO planDAO;

	public List<Plan> list() {

		return planDAO.findAll();

	}

	public Plan getPlan(Integer id) {

		return planDAO.find(id);

	}

	public Plan create(PlanForm planForm) {

		return planDAO.save(planForm);
	}

	public Plan update(PlanForm planForm) {

		return planDAO.update(planForm);

	}

	public void delete(Integer id) {

		this.planDAO.delete(id);

	}

}

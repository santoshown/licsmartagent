package com.paramountit.licsmart.services;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paramountit.licsmart.dao.UserRegistrationDAO;
import com.paramountit.licsmart.entity.User;
import com.paramountit.licsmart.forms.UserRegistrationForm;

@Service
public class UserRegistrationService {

	@Autowired
	private UserRegistrationDAO userRegistrationDAO;

	public Boolean checkUserExist(UserRegistrationForm form) {

		return userRegistrationDAO.checkUserExist(form);

	}

	public User createUser(UserRegistrationForm form) throws ParseException {

		return userRegistrationDAO.createUser(form);

	}

}

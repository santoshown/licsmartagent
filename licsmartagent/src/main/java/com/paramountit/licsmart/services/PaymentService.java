package com.paramountit.licsmart.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paramountit.licsmart.dao.PaymentDAO;
import com.paramountit.licsmart.entity.PaymentType;
import com.paramountit.licsmart.forms.PaymentForm;

@Service
public class PaymentService {

	@Autowired
	private PaymentDAO paymentDAO;

	public List<PaymentType> findAll() {

		return paymentDAO.findAll();
	}

	public PaymentType getPaymentType(Integer id) {

		return paymentDAO.find(id);
	}

	public PaymentType save(PaymentForm form) {

		return paymentDAO.save(form);
	}

	public PaymentType update(PaymentForm form) {

		return paymentDAO.update(form);
	}

	public void delete(Integer id) {

		paymentDAO.delete(id);
	}

}

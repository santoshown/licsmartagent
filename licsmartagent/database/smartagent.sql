CREATE DATABASE  IF NOT EXISTS `smartagent` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smartagent`;
-- MySQL dump 10.14  Distrib 5.5.39-MariaDB, for Linux (i686)
--
-- Host: 127.0.0.1    Database: smartagent
-- ------------------------------------------------------
-- Server version	5.5.39-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MstChannelType`
--

DROP TABLE IF EXISTS `MstChannelType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MstChannelType` (
  `ChannelID` int(11) NOT NULL AUTO_INCREMENT,
  `ChannelName` varchar(45) NOT NULL,
  PRIMARY KEY (`ChannelID`),
  UNIQUE KEY `channel_name_UNIQUE` (`ChannelName`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MstChannelType`
--

LOCK TABLES `MstChannelType` WRITE;
/*!40000 ALTER TABLE `MstChannelType` DISABLE KEYS */;
INSERT INTO `MstChannelType` VALUES (2,'ese'),(1,'ssE');
/*!40000 ALTER TABLE `MstChannelType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MstCustomerRegistration`
--

DROP TABLE IF EXISTS `MstCustomerRegistration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MstCustomerRegistration` (
  `CustomerID` int(11) unsigned NOT NULL,
  `FirstName` varchar(45) NOT NULL,
  `Mname` varchar(45) DEFAULT NULL,
  `LastName` varchar(45) NOT NULL,
  `PolicyNo` varchar(45) NOT NULL,
  `DOC` date NOT NULL,
  `SumAssured` float NOT NULL,
  `Nom` varchar(45) DEFAULT NULL,
  `AgentName` varchar(45) NOT NULL,
  `DOB` date NOT NULL,
  `Phone` int(11) NOT NULL,
  `EmailID` varchar(45) DEFAULT NULL,
  `AddedOn` date NOT NULL,
  `EnrollerID` int(11) NOT NULL,
  `PremiumID` int(11) NOT NULL,
  `PlanID` int(11) NOT NULL,
  `PaymentTypeID` int(11) NOT NULL,
  `ChannelID` int(11) NOT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`),
  KEY `fk_MstCustomerRegistration_User1_idx` (`EnrollerID`),
  KEY `fk_MstCustomerRegistration_MstPremium1_idx` (`PremiumID`),
  KEY `fk_MstCustomerRegistration_MstPlan1_idx` (`PlanID`),
  KEY `fk_MstCustomerRegistration_MstPaymentType1_idx` (`PaymentTypeID`),
  KEY `fk_MstCustomerRegistration_MstChannelType1_idx` (`ChannelID`),
  CONSTRAINT `fk_MstCustomerRegistration_MstChannelType1` FOREIGN KEY (`ChannelID`) REFERENCES `MstChannelType` (`ChannelID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MstCustomerRegistration_MstPaymentType1` FOREIGN KEY (`PaymentTypeID`) REFERENCES `MstPaymentType` (`PaymentTypeID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MstCustomerRegistration_MstPlan1` FOREIGN KEY (`PlanID`) REFERENCES `MstPlan` (`PlanID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MstCustomerRegistration_MstPremium1` FOREIGN KEY (`PremiumID`) REFERENCES `MstPremium` (`PremiumID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_MstCustomerRegistration_User1` FOREIGN KEY (`EnrollerID`) REFERENCES `MstUser` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MstCustomerRegistration`
--

LOCK TABLES `MstCustomerRegistration` WRITE;
/*!40000 ALTER TABLE `MstCustomerRegistration` DISABLE KEYS */;
/*!40000 ALTER TABLE `MstCustomerRegistration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MstPaymentType`
--

DROP TABLE IF EXISTS `MstPaymentType`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MstPaymentType` (
  `PaymentTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `PaymentType` varchar(45) NOT NULL,
  PRIMARY KEY (`PaymentTypeID`),
  UNIQUE KEY `payment_type_UNIQUE` (`PaymentType`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MstPaymentType`
--

LOCK TABLES `MstPaymentType` WRITE;
/*!40000 ALTER TABLE `MstPaymentType` DISABLE KEYS */;
INSERT INTO `MstPaymentType` VALUES (1,'Cash');
/*!40000 ALTER TABLE `MstPaymentType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MstPlan`
--

DROP TABLE IF EXISTS `MstPlan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MstPlan` (
  `PlanID` int(11) NOT NULL AUTO_INCREMENT,
  `PlanName` varchar(45) NOT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `IsActive` int(11) DEFAULT NULL,
  PRIMARY KEY (`PlanID`),
  UNIQUE KEY `Plan_name_UNIQUE` (`PlanName`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MstPlan`
--

LOCK TABLES `MstPlan` WRITE;
/*!40000 ALTER TABLE `MstPlan` DISABLE KEYS */;
INSERT INTO `MstPlan` VALUES (1,'Jeevan sathi','Jeevan sathi',2);
/*!40000 ALTER TABLE `MstPlan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MstPremium`
--

DROP TABLE IF EXISTS `MstPremium`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MstPremium` (
  `PremiumID` int(11) NOT NULL AUTO_INCREMENT,
  `Premium` varchar(45) NOT NULL,
  PRIMARY KEY (`PremiumID`),
  UNIQUE KEY `premium_UNIQUE` (`Premium`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MstPremium`
--

LOCK TABLES `MstPremium` WRITE;
/*!40000 ALTER TABLE `MstPremium` DISABLE KEYS */;
INSERT INTO `MstPremium` VALUES (1,'Montly');
/*!40000 ALTER TABLE `MstPremium` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MstUser`
--

DROP TABLE IF EXISTS `MstUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MstUser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` text NOT NULL,
  `password` text NOT NULL,
  `IsActive` int(11) DEFAULT NULL,
  `FirstName` varchar(45) NOT NULL,
  `Mname` varchar(45) DEFAULT NULL,
  `LastName` varchar(45) NOT NULL,
  `Phone` bigint(20) DEFAULT NULL,
  `Address` varchar(45) DEFAULT NULL,
  `AddedOn` date DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `Reporting` varchar(45) DEFAULT NULL,
  `AgentNumber` varchar(45) NOT NULL,
  `EmailId` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MstUser`
--

LOCK TABLES `MstUser` WRITE;
/*!40000 ALTER TABLE `MstUser` DISABLE KEYS */;
INSERT INTO `MstUser` VALUES (7,'admin','46391ccf1779a60ce8e25a6ab7bba6c1439fa94406e02168e3c04f41abce9b54609799426e070df2',0,'',NULL,'',NULL,NULL,NULL,'0000-00-00',NULL,'',NULL),(8,'user','8aa0ad6a6ce2c021887ea4209467630afb80ed403935f65969d92e6e01f3f950a0724aa35bb5009b',0,'',NULL,'',NULL,NULL,NULL,'0000-00-00',NULL,'',NULL),(9,'santosh','70c1bd4043768442ed41309e099e0b4f320a42958b2b590bc96e28d3e93edef89ee42e02d65c6a98',NULL,'santosh','v','Rode',NULL,'Manjari',NULL,NULL,'123456','sdsds','santoshrode19@gmail.com'),(10,'vikram','3682c5876356ce4ce09dc50d25d17088f727286934f8d63c24859ae4920cce0f6b53b0515686a7c7',NULL,'vikram','b','bbv',9686062891,'Manjari',NULL,NULL,'123456','sdsds','rerer@gmail.com'),(11,'pradip','c609115ab19bcdf36965fac0ec1c6b7f8a02153da51ffef58616597b18cc1854d549a46b44ba88c3',NULL,'pradip','a','fff',9686062891,'Manjari','2015-05-28',NULL,'123456','sdsds','dsdd@gmail.com'),(12,'swapnil','3739bf32faf39a30ed6c9a803690a85475712f187961e602f46c0c24e332676c9a02b1ce16621999',NULL,'swapnil','d','shinde',9999999999,'karad','2015-05-28','1993-01-06','s','d','fdfd@gmail.com');
/*!40000 ALTER TABLE `MstUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `User_roles`
--

DROP TABLE IF EXISTS `User_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User_roles` (
  `User_id` int(11) NOT NULL,
  `Description` varchar(45) DEFAULT NULL,
  `roles` varchar(255) NOT NULL,
  KEY `fk_User_roles_User1_idx` (`User_id`),
  CONSTRAINT `fk_User_roles_User1` FOREIGN KEY (`User_id`) REFERENCES `MstUser` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User_roles`
--

LOCK TABLES `User_roles` WRITE;
/*!40000 ALTER TABLE `User_roles` DISABLE KEYS */;
INSERT INTO `User_roles` VALUES (7,'admin','admin'),(8,'user','user'),(9,NULL,'user'),(10,NULL,'user'),(11,NULL,'user'),(12,NULL,'user');
/*!40000 ALTER TABLE `User_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-29 13:41:05